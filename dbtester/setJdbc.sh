#!/bin/bash

if [[ -z $jdbc_url ]] || [[ "$jdbc_url" == "" ]]; then
   if [[ -z $db_vendor ]] || [[ "$db_vendor" == "" ]]; then
      echo ""; read -p "Enter Database Vendor: " db_vendor
   fi
   if [[ $db_vendor = "db2" ]]; then
   # Handle Db2 JDBC
      if [[ -z $db_name ]] || [[ "$db_name" == "" ]]; then
         read -p "Enter Database: " db_name
      fi
      if [[ -z $clientProgramName ]] || [[ "$clientProgramName" == "" ]]; then
         read -p "Enter clientProgramName: " clientProgramName
      fi
      if [[ -z $connectionTimeout ]] || [[ "$connectionTimeout" == "" ]]; then
         read -p "Enter connectionTimeout: " connectionTimeout
      fi      
      if [[ -z $maxRetriesForClientReroute ]] || [[ "$maxRetriesForClientReroute" == "" ]]; then
         read -p "Enter maxRetriesForClientReroute: " maxRetriesForClientReroute
      fi
      if [[ -z $retryIntervalForClientReroute ]] || [[ "$retryIntervalForClientReroute" == "" ]]; then
         read -p "Enter retryIntervalForClientReroute: " retryIntervalForClientReroute
      fi
      if [[ -z $primary_host ]] || [[ "$primary_host" == "" ]]; then
         read -p "Enter primary_host: " primary_host
      fi
      if [[ -z $standby_host ]] || [[ "$standby_host" == "" ]]; then
         read -p "Enter standby_host: " standby_host
      fi
      if [[ -z $primary_port ]] || [[ "$primary_port" == "" ]]; then
         read -p "Enter primary_port: " primary_port
      fi
      if [[ -z $standby_port ]] || [[ "$standby_port" == "" ]]; then
         read -p "Enter standby_port: " standby_port
      fi
      echo " Please select for connection option:"
      echo "   select 1) TCP"
      echo "   select 2) SSL"
      read -p "Select #: " jdbc_url_select
      echo "jdbc_url_select=$jdbc_url_select"
      case ${jdbc_url_select} in
         "1") jdbc_url="jdbc:db2://$primary_host:$primary_port/$db_name:clientProgramName=$clientProgramName;connectionTimeout=$connectionTimeout;maxRetriesForClientReroute=$maxRetriesForClientReroute;retryIntervalForClientReroute=$retryIntervalForClientReroute;clientRerouteAlternateServerName=$primary_host,$standby_host;clientRerouteAlternatePortNumber=$primary_port,$standby_port;enableSeamlessFailover=true;enableClientAffinitiesList=1;"
         ;;
         "2") jdbc_url="jdbc:db2://$primary_host:$primary_port/$db_name:clientProgramName=$clientProgramName;connectionTimeout=$connectionTimeout;maxRetriesForClientReroute=$maxRetriesForClientReroute;retryIntervalForClientReroute=$retryIntervalForClientReroute;clientRerouteAlternateServerName=$primary_host,$standby_host;clientRerouteAlternatePortNumber=$primary_port,$standby_port;enableSeamlessFailover=true;enableClientAffinitiesList=1;sslConnection=true;"
         ;;
      esac
      echo jdbc_url: $jdbc_url
   fi
fi