package xyz.ymoon.dbtester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Run {
    public static void main(String[] args) {
        System.out.println("**** Starting RunTest...");
        List<String> argslist = new ArrayList<>(Arrays.asList(args));
        //System.out.println("args: "+argslist);
        String runType = argslist.get(0);

        if (runType.equals("connect_db2")) {
            argslist.remove(0);
            //System.out.println("args: "+argslist.toArray(new String[0]));
            TestConnDb2.runTestConnDb2(argslist.toArray(String[]::new));
        } else if (runType.equals("connect_keep_db2")) {
            argslist.remove(0);
            //System.out.println("args: "+argslist.toArray(new String[0]));
            TestConnKeepDb2.runTestConnKeepDb2(argslist.toArray(String[]::new));
        } else if (runType.equals("connect_pool_db2")) {
            argslist.remove(0);
            //System.out.println("args: "+argslist.toArray(new String[0]));
            TestConnPoolDb2.runTestConnPoolDb2(argslist.toArray(String[]::new));
        }
    }
}