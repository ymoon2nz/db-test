package xyz.ymoon.dbtester;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import xyz.ymoon.db.HikariCPDataSource;

public class TestConnPoolDb2 extends Thread {
	public long start;
	public long end;
	public Connection conn;
	public int runSqlNum = 2;
	public int delay = 1000;		// milliseconds
	public String runSql = "select mon_get_application_id() || CURRENT_TIMESTAMP col1 from sysibm.sysdummy1";
	
	public TestConnPoolDb2 (Connection conn, int runSqlNum, int delay, String runSql) {

		this.conn = conn;
		this.runSqlNum = runSqlNum;
		this.delay = delay;
		if (runSql != null && !runSql.equals("")) this.runSql = runSql;
		
	}

    public static void main(String[] args) {
        runTestConnPoolDb2(args);
    }

    public static void runTestConnPoolDb2(String[] args) {
        long time = System.currentTimeMillis();
        long totalElapsed = 0l;
        long elapsed = 0l;
        
        int connNum = 2;
        int runSqlNum = 2;
        int delay = 1000;	// milliseconds
        String runSql = "";
        String url = "";
        String username = "";
        String password = "";
        try {
        	url = args[0];
        	username = args[1];
        	password = args[2];
        } catch (Exception e) {}
        try {runSql = args[3];} catch (Exception e) {}
        try {connNum = Integer.parseInt(args[4]);} catch (Exception e) {}
        try {delay = Integer.parseInt(args[5]);} catch (Exception e) {}
        try {runSqlNum = Integer.parseInt(args[6]);} catch (Exception e) {}
        
    	//Connection[] conns;
        ArrayList<TestConnPoolDb2> testThreads = new ArrayList<TestConnPoolDb2>();
        HikariCPDataSource hds = new HikariCPDataSource(url, username, password);
        
        for (int i=0; i < connNum; i++) {
        	TestConnPoolDb2 thread;
			try {
				thread = new TestConnPoolDb2(hds.getConnection(), runSqlNum, delay, runSql);
				testThreads.add(thread);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	//thread.start();
        }
        
        Iterator<TestConnPoolDb2> it = testThreads.iterator();
        while (it.hasNext()) {
        	it.next().start();
        }
        
        System.out.println(" ****** End of Run ****** ");
    }
    
    public void run() {
    	try {
    		long threadId = Thread.currentThread().getId();
            System.out.println("["+threadId+"] loop started ...");
            start = System.currentTimeMillis();
            String firstValue = "";
            for (int i=0; i < runSqlNum; i++) {
                try {
                    Thread.currentThread().sleep(delay);
                } catch (Exception e) {}
                
                //time = System.currentTimeMillis();
                firstValue = testQuery( conn, runSql );
                //System.out.println("Closing["+i+"] Elapsed(ms): " + elapsed );
            }
            end = System.currentTimeMillis();
            conn.close();
            System.out.println("["+threadId+"] Total ("+runSqlNum+") Elapsed(ms): " + (end - start) + " - "+firstValue);
            //conn.setTransactionIsolation(DEFAULT_ISOLATION);
            //conn.setReadOnly(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String testQuery(Connection conn, String sql) throws SQLException {
        Statement stmt = conn.createStatement();
        //System.out.println("sql: "+sql);
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        String firstValue = "";
        while (rs.next()) {
            //System.out.println("ROW(column-1): "+rs.getString(1));
            firstValue = rs.getString(1);
        }
        stmt.close();
        return firstValue;
    }
}
