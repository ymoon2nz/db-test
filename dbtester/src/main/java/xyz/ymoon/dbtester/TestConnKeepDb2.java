package xyz.ymoon.dbtester;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import java.util.ArrayList;

public class TestConnKeepDb2 extends Thread {
    public static void main(String[] args) {
        runTestConnKeepDb2(args);
    }
    public static void runTestConnKeepDb2(String[] args) {
        long time = System.currentTimeMillis();
        long totalElapsed = 0l;
        long elapsed = 0l;
        String query = "select mon_get_application_id() || CURRENT_TIMESTAMP col1 from sysibm.sysdummy1";
        
        try {
        	ArrayList<Connection> conns = new ArrayList<Connection>();
            Connection conn;
            Class.forName("com.ibm.db2.jcc.DB2Driver");
            Properties prop = new Properties();
            String url = args[0];
            prop.put("user", args[1]);
            prop.put("password", args[2]);
            System.out.println("...getting Connection");

            int loopNum = 1;
            int delay = 1000;
            try {
                if (args[3] !=null) {
                    query = args[3];
                }
                loopNum = Integer.parseInt(args[4]);
            } catch (Exception e) {}
            try {
                delay = Integer.parseInt(args[5]);
            } catch (Exception e) {}
            System.out.println("loop started with: "+query);

            for (int i=0; i < loopNum; i++) {
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {}
                
                time = System.currentTimeMillis();
                conns.add(DriverManager.getConnection(url, prop));
                elapsed = System.currentTimeMillis() -  time;
                totalElapsed += elapsed;
                System.out.println("Connecting["+i+"] Elapsed(ms): " + elapsed );
            }
            System.out.println("Connecting Total Elapsed(ms): " + totalElapsed );
            totalElapsed = 0l;
            try {
                Thread.sleep(12000);
            } catch (Exception e) {}
            for (int i=0; i < conns.size(); i++) {
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {}
                
                time = System.currentTimeMillis();
                testQuery( conns.get(i), query );
                conns.get(i).close();
                elapsed = System.currentTimeMillis() -  time;
                totalElapsed += elapsed;
                conns.remove(i);
                //System.out.println("Closing["+i+"] Elapsed(ms): " + elapsed );
            }
            System.out.println("Closing Total Elapsed(ms): " + totalElapsed );
            //conn.setTransactionIsolation(DEFAULT_ISOLATION);
            //conn.setReadOnly(true);
        } catch (ClassNotFoundException cnfe) {
            try {
                throw new SQLException("Can't find class for driver: com.ibm.db2.jcc.DB2Driver");
            } catch (SQLException e) {e.printStackTrace();}
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ****** End of Run ****** ");
    }

    public static void testQuery(Connection conn, String sql) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        while (rs.next()) {
            System.out.println("ROW(column-1): "+rs.getString(1));
        }
        stmt.close();
    }
}
