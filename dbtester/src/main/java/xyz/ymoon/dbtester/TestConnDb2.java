package xyz.ymoon.dbtester;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

public class TestConnDb2 {
    public static void main(String[] args) {
        runTestConnDb2(args);
    }

    public static void runTestConnDb2(String[] args) {
        long time = System.currentTimeMillis();
        String query = "select CURRENT_TIMESTAMP from sysibm.sysdummy1";
        Connection conn;
        try {
            Class.forName("com.ibm.db2.jcc.DB2Driver");
            Properties prop = new Properties();
            String url = args[0];
            prop.put("user", args[1]);
            prop.put("password", args[2]);
            System.out.println("...getting Connection");
            conn = DriverManager.getConnection(url, prop);
            System.out.println("Connection Elapsed(ms): "+ (System.currentTimeMillis() -  time) );
            int loopNum = 1;
            int delay = 1000;
            try {
                if (args[3] !=null) {
                    query = args[3];
                }
                loopNum = Integer.parseInt(args[4]);
            } catch (Exception e) {}
            try {
                delay = Integer.parseInt(args[5]);
            } catch (Exception e) {}

            System.out.println("loop started with: "+query);
            for (int i=0; i < loopNum; i++) {
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {}
                
                time = System.currentTimeMillis();
                testQuery(conn, query);
                System.out.println("Query Elapsed(ms): "+ (System.currentTimeMillis() -  time) );
            }
            
            //conn.setTransactionIsolation(DEFAULT_ISOLATION);
            //conn.setReadOnly(true);
        } catch (ClassNotFoundException cnfe) {
            try {
                throw new SQLException("Can't find class for driver: com.ibm.db2.jcc.DB2Driver");
            } catch (SQLException e) {e.printStackTrace();}
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ****** End of Run ****** ");
    }

    public static void testQuery(Connection conn, String sql) throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        while (rs.next()) {
            System.out.println("ROW(column-1): "+rs.getString(1));
        }
        stmt.close();
    }
}
