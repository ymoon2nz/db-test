package xyz.ymoon.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class HikariCPDataSource {

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    //static {
        // config = new HikariConfig("datasource.properties");

        // Properties props = new Properties();
        // props.setProperty("dataSourceClassName", "org.h2.Driver");
        // props.setProperty("dataSource.user", "");
        // props.setProperty("dataSource.password", "");
        // props.put("dataSource.logWriter", new PrintWriter(System.out));
        // config = new HikariConfig(props);



        // ds.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;INIT=runscript from 'classpath:/db.sql'");
        // ds.setUsername("");
        // ds.setPassword("");
    //}

    public HikariCPDataSource(String url, String username, String password) {
        //config = new HikariConfig("datasource.properties");
        Properties props = new Properties();
        
        if (url.matches("jdbc:db2:(.*)")) {
        	//"com.ibm.db2.jcc.DB2Driver"
            //props.setProperty("dataSourceClassName", "com.ibm.db2.jcc.DB2Driver");
        	
        	props.setProperty("dataSource.user", username);
            props.setProperty("dataSource.password", password);
            props.put("dataSource.logWriter", new PrintWriter(System.out));
            config = new HikariConfig(props);
            config.setDriverClassName("com.ibm.db2.jcc.DB2Driver");
            config.setJdbcUrl(url);
            config.addDataSourceProperty("cachePrepStmts", "true");
            //config.addDataSourceProperty("prepStmtCacheSize", "250");
            //config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        }
        ds = new HikariDataSource(config);
    	
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

}