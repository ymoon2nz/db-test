#!/bin/bash
############################################################
############################################################
# Main program                                             #
# runConnTest.sh                                           #
############################################################
############################################################
############################################################
# Process the input options. Add options as needed.        #
############################################################
this_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Syntax: runConnTest.sh -j {jdbc_url} -u {user} -p {password} -q {SQL} -c {attempt} -d {delay_ms}"
while getopts r:j:u:p:q:c:d: flag
do
    case "${flag}" in
      j) jdbc_url=${OPTARG};;
      u) db_user=${OPTARG};;
      p) db_pass=${OPTARG};;
      q) db_sql=${OPTARG};;
      c) attempt_count=${OPTARG};;
      d) delay_ms=${OPTARG};;
    esac
done

echo "jdbc_url: $jdbc_url";
echo "db_user: $db_user";
echo "db_pass: $db_pass";
echo "query_str: $query_str";
echo "attempt_count: $attempt_count";
echo "delay_ms: $delay_ms";

# Credential
if [[ -z $db_user ]] || [[ "$db_user" == "" ]]; then
   echo ""; read -p "Enter user ($db_user): " db_user
fi
if [[ -z $db_pass ]] || [[ "$db_pass" == "" ]]; then
   read -s -p "Enter password: " db_pass
fi

#echo "jdbc:db2://$primary_host:$primary_port/$db_name:clientProgramName=$api_name;connectionTimeout=$connectionTimeout;maxRetriesForClientReroute=$maxRetriesForClientReroute;retryIntervalForClientReroute=$retryIntervalForClientReroute;clientRerouteAlternateServerName=$primary_host,$standby_host;clientRerouteAlternatePortNumber=$primary_port,$standby_port;enableSeamlessFailover=true;enableClientAffinitiesList=1;"
# JDBC URL Parameters

if [[ -z $jdbc_url ]] || [[ "$jdbc_url" == "" ]]; then
   . $this_path/setJdbc.sh
else 
   db_vendor=$( echo $jdbc_url | awk -F ":" '{print $2}' )
fi


if [[ -z $db_sql ]] || [[ "$db_sql" == "" ]]; then
   read -p "Enter SQL: " db_sql
fi
if [[ -z $attempt_count ]]; then
   read -p "Enter Attempt(s): " attempt_count
fi
if [[ -z $delay_ms ]]; then
   read -p "Enter Delay(ms): " delay_ms
fi

date
echo java -jar $this_path/target/dbtester-spring-boot.jar "connect_"${db_vendor} $jdbc_url "$db_user" "*********" \
   \"${db_sql}\" ${attempt_count} ${delay_ms}

java -jar $this_path/target/dbtester-spring-boot.jar "connect_"${db_vendor} $jdbc_url "$db_user" "$db_pass" \
   "${db_sql}" ${attempt_count} ${delay_ms}
