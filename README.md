# Database Test #

* Quick summary

This repository contains various Database Test codes and set of programs for testing Database including connectivity.

* Version : 0.1 (2022-08)

* [db-test](https://bitbucket.org/ymoon2nz/db-test/)

--- 

## dbtester ##

* Build
```
mvn package
```

* Run - connect_db2
```
# Set JDBC url
jdbc_url="jdbc:db2://localhost:50000/DOCKER"
db_user=db2inst1
db_pass=admin
db_sql="select count(tabname) tab_count from syscat.tables"

# Generate 20 connections with 500 ms intervals
./dbtester/runConnTest.sh -j "$jdbc_url" -u $db_user -p "$db_pass" -q "$db_sql" -c 20 -d 500
```

* Run - connect_keep_db2
```
# Set JDBC url
jdbc_url="jdbc:db2://localhost:50000/DOCKER"
db_user=db2inst1
db_pass=admin
db_sql="select count(tabname) tab_count from syscat.tables"

# Generate 20 connections with 500 ms intervals and keep until all finished
./dbtester/runConnKeepTest.sh -j "$jdbc_url" -u $db_user -p "$db_pass" -q "$db_sql" -c 20 -d 500
```

--- 
